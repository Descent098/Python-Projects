import requests
import bs4

def main():
    """Main Run loop"""
    print("=======================-Weather Client-=======================\n")
    #TODO Add postal code functionality

    choice = eval(input("Would you like to get the weather from (1)zipcode or (2)Country Code + City?: "))
    if choice == 1:
        #Get zipcode from user
        zipcode = input("What zipcode do you want the weather for (90210)>: ")
        #get html from web using zipcode
        html = get_html_from_zip(zipcode)

    elif choice == 2:
        #Get country code from user
        country_code = input("What country code do you want the weather for i.e. gb for great brittan etc.: ")
        #Get city from user
        city = input("What city do you want the weather for i.e. london: ")
        #get html from web using country code + city
        html = get_html_from_id(country_code, city)

    #Parse html
    weather = get_weather(html)

def get_html_from_zip(zipcode):
    url = 'http://www.wunderground.com/weather-forecast/{}'.format(zipcode)
    response = requests.get(url)
    return response.text

def get_html_from_id(country_code,city):
    url = 'http://www.wunderground.com/weather/{}/{}'.format(country_code,city)
    response = requests.get(url)
    return response.text

def get_weather(html):
    # cityCss = '.region-content-header h1'
    # weatherScaleCss = '.wu-unit-temperature .wu-label'
    # weatherTempCss = '.wu-unit-temperature .wu-value'
    # weatherConditionCss = '.condition-icon'

    soup = bs4.BeautifulSoup(html, 'html.parser')
    loc = soup.find(class_='region-content-header').find('h1').get_text()
    condition = soup.find(class_='condition-icon').get_text()
    temp = soup.find(class_='wu-unit-temperature').find(class_='wu-value').get_text()
    scale = soup.find(class_='wu-unit-temperature').find(class_='wu-label').get_text()

    print("Location: {}\nCondition: {}\nTemperature: {}{}".format(loc.strip("\n "), condition.strip("\n "), temp.strip("\n "), scale.strip("\n ")))

if __name__ == "__main__":
    main()
